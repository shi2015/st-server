package com.st.rest;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.st.common.core.BaseController;
import com.st.common.core.ResultEntity;
import com.st.common.utils.StringUtils;
import com.st.system.domain.SysUser;
import com.st.system.dto.SysUserDTO;
import com.st.system.service.ISysUserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Api(value = "用户管理md")
@RestController
@RequiredArgsConstructor
@RequestMapping("/user")
public class SysUserController extends BaseController {

    @Autowired
    private ISysUserService sysUserService;

    @ApiOperation("查询用户")
    @GetMapping
    /*@PreAuthorize("@el.check('user:list')")*/
    public ResultEntity query(SysUserDTO params){
        QueryWrapper<SysUser> queryWrapper = new QueryWrapper<>();
        if (StringUtils.isNotEmpty(params.getNickName())){
            queryWrapper.like("nick_name", params.getNickName());
        }
        if (StringUtils.isNotEmpty(params.getUserName())) {
            queryWrapper.eq("user_name", params.getUserName());
        }
        IPage<SysUser> list = sysUserService.page(makePageInfo(params),queryWrapper);
        //return ResultEntity.success(list);
        return ResultEntity.success(toPage(list));
    }
}
