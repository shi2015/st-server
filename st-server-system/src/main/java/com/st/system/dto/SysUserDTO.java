package com.st.system.dto;

import com.st.system.domain.SysUser;
import lombok.Data;

/**
 * @author ssw
 * @version 1.0
 * @date 2020/12/8 15:23
 */
@Data
public class SysUserDTO extends SysUser {
}
