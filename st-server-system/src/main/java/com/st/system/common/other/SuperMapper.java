package com.st.system.common.other;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @author ssw
 * @version 1.0
 * @date 2020/12/2 17:18
 *  mapper 父类，注意这个类不要让 mp 扫描到！！
 */
public interface SuperMapper<T> extends BaseMapper<T> {
    // 这里可以放一些公共的方法
}
