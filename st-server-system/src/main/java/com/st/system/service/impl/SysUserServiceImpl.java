package com.st.system.service.impl;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.st.system.domain.SysLogininfo;
import com.st.system.domain.SysUser;
import com.st.system.mapper.SysLogininfoMapper;
import com.st.system.mapper.SysUserMapper;
import com.st.system.service.ISysLogininfoService;
import com.st.system.service.ISysUserService;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author ssw
 * @version 1.0
 * @date 2020/12/2 17:13
 */
@Service
public class SysUserServiceImpl extends ServiceImpl<SysUserMapper, SysUser> implements ISysUserService {
    @Override
    public boolean deleteAll() {
        return retBool(baseMapper.deleteAll());
    }

    @Override
    public List<SysUser> selectListBySQL() {
        return baseMapper.selectListBySQL();
    }

    @Override
    public List<SysUser> selectListByWrapper(Wrapper wrapper) {
        return baseMapper.selectListByWrapper(wrapper);
    }

    @Override
    public SysUser selectUserByUserName(String userName) {
        return baseMapper.selectUserByUserName(userName);
    }
}
