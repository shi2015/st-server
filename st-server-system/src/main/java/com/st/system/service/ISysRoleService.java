package com.st.system.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.st.system.domain.SysRole;

import java.util.Set;

/**
 * 业务层
 */
public interface ISysRoleService extends IService<SysRole>{
    /**
     * 根据用户ID查询角色
     *
     * @param userId 用户ID
     * @return 权限列表
     */
    public Set<String> selectRolePermissionByUserId(Long userId);
}
