package com.st.system.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.st.system.domain.SysLogininfo;
import com.st.system.mapper.SysLogininfoMapper;
import com.st.system.service.ISysLogininfoService;
import org.springframework.stereotype.Service;

/**
 * @author ssw
 * @version 1.0
 * @date 2020/12/2 17:14
 */
@Service
public class SysLogininfoServiceImpl extends ServiceImpl<SysLogininfoMapper, SysLogininfo> implements ISysLogininfoService {
}
