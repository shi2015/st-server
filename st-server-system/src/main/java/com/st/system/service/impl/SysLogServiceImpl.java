package com.st.system.service.impl;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.st.system.domain.SysLog;
import com.st.system.domain.SysUser;
import com.st.system.mapper.SysLogMapper;
import com.st.system.mapper.SysUserMapper;
import com.st.system.service.ISysLogService;
import com.st.system.service.ISysUserService;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author ssw
 * @version 1.0
 * @date 2020/12/2 17:13
 */
@Service
public class SysLogServiceImpl extends ServiceImpl<SysLogMapper, SysLog> implements ISysLogService {
}
