package com.st.system.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.st.system.domain.SysLog;

/**
 * 业务层
 */
public interface ISysLogService extends IService<SysLog>{
}
