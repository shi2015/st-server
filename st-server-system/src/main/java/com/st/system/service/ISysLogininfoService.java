package com.st.system.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.st.system.domain.SysLogininfo;

/**
 * 系统访问日志情况信息 服务层
 */
public interface ISysLogininfoService extends IService<SysLogininfo> {
}
