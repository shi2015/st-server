package com.st.system.service;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.extension.service.IService;
import com.st.system.domain.SysUser;

import java.util.List;

/**
 * 用户 业务层
 */
public interface ISysUserService extends IService<SysUser>{
    boolean deleteAll();

    List<SysUser> selectListBySQL();

    List<SysUser> selectListByWrapper(Wrapper wrapper);

    SysUser selectUserByUserName(String userName);
}
