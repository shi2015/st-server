package com.st.system.mapper;

import com.st.system.common.other.SuperMapper;
import com.st.system.domain.SysMenu;

import java.util.List;

/**
 * 用户表 数据层
 * 
 */
public interface SysMenuMapper extends SuperMapper<SysMenu> {
    /**
     * 根据用户ID查询菜单
     *
     * @param userId 用户ID
     * @return 菜单列表
     */
    public List<SysMenu> selectMenuTreeByUserId(Long userId);
    /**
     * 根据用户ID查询权限
     *
     * @param userId 用户ID
     * @return 权限列表
     */
    public List<String> selectMenuPermsByUserId(Long userId);
    /**
     * 根据用户ID查询菜单
     *
     * @return 菜单列表
     */
    public List<SysMenu> selectMenuTreeAll();
}
