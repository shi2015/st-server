package com.st.system.mapper;

import com.st.system.common.other.SuperMapper;
import com.st.system.domain.SysLog;

/**
 * 用户表 数据层
 * 
 */
public interface SysLogMapper extends SuperMapper<SysLog> {
}
