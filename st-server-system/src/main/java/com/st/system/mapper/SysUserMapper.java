package com.st.system.mapper;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.st.system.common.other.SuperMapper;
import com.st.system.domain.SysUser;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * 用户表 数据层
 * 
 */
public interface SysUserMapper extends SuperMapper<SysUser> { //extends BaseMapper<SysUser>
    /**
     * 自定义注入方法
     */
    int deleteAll();

    @Select("select test_id as id, name, age, test_type from sys_user")
    List<SysUser> selectListBySQL();

    List<SysUser> selectListByWrapper(@Param("ew") Wrapper wrapper);

    SysUser selectUserByUserName(@Param("userName") String userName);
}
