package com.st.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.st.system.domain.SysLogininfo;

/**
 * 系统访问日志情况信息 数据层
 */
public interface SysLogininfoMapper extends BaseMapper<SysLogininfo> {
}
