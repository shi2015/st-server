package com.st.system.mapper;

import com.st.system.common.other.SuperMapper;
import com.st.system.domain.SysRole;

import java.util.List;

/**
 * 用户表 数据层
 * 
 */
public interface SysRoleMapper extends SuperMapper<SysRole> {
    /**
     * 根据用户ID查询角色
     *
     * @param userId 用户ID
     * @return 角色列表
     */
    public List<SysRole> selectRolePermissionByUserId(Long userId);
}
