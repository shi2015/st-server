package com.st.common.core;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.metadata.OrderItem;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.st.common.utils.DateUtils;
import com.st.common.utils.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;

import java.beans.PropertyEditorSupport;
import java.util.*;

/**
 * @author ssw
 * @version 1.0
 * @date 2020/12/3 10:35
 */
public class BaseController {
    protected final Logger logger = LoggerFactory.getLogger(BaseController.class);

    /**
     * 将前台传递过来的日期格式的字符串，自动转化为Date类型
     */
    @InitBinder
    public void initBinder(WebDataBinder binder)
    {
        // Date 类型转换
        binder.registerCustomEditor(Date.class, new PropertyEditorSupport()
        {
            @Override
            public void setAsText(String text)
            {
                setValue(DateUtils.parseDate(text));
            }
        });
    }

    /**
     * 响应返回结果
     *
     * @param rows 影响行数
     * @return 操作结果
     */
    protected ResultEntity toAjax(int rows)
    {
        return rows > 0 ? ResultEntity.success() : ResultEntity.error();
    }

    /**
     * 页面跳转
     */
    public String redirect(String url)
    {
        return StringUtils.format("redirect:{}", url);
    }

    protected static Page makePageInfo(BaseEntity entity) {
        Page pageInfo = new Page();
        pageInfo.setCurrent(entity.getPage() == 0 ? 1 : entity.getPage());
        pageInfo.setSize(entity.getSize() == 0 ? 10 : entity.getPage());
        String[] sorts = entity.getSort();
        if(sorts!=null&&sorts.length>0){
            List<OrderItem> orders = new ArrayList<>();
            if(sorts.length == 2 && sorts[0].indexOf(",") == -1 && sorts[1].indexOf(",") == -1 && ("ASC".equals(sorts[1].toUpperCase()) || "DESC".equals(sorts[1].toUpperCase()))) {
                // 长度是2 并且每个数据都不含逗号 并且第二个参数是排序方式，这种情况直接排序
                orders.add(new OrderItem(sorts[0],"ASC".equals(sorts[1].toUpperCase())));
            } else {
                for (int i=0;i<sorts.length;i++){
                    String[] sort = sorts[i].split(",");
                    if(sort.length == 1) {
                        orders.add(new OrderItem(sort[0],true));
                    } else if(sort.length == 2) {
                        orders.add(new OrderItem(sort[0],"ASC".equals(sort[1].toUpperCase())));
                    }
                }
            }
            pageInfo.setOrders(orders);
        }
        return pageInfo;
    }

    /**
     * 转换结果
     * @param iPage
     * @return
     */
    protected static Map toPage(IPage iPage) {
        Map map = new HashMap();
        map.put("records", iPage.getRecords());
        map.put("total", iPage.getTotal());
        map.put("size", iPage.getSize());
        map.put("current", iPage.getCurrent());
        map.put("pages", iPage.getPages());
        return map;
    }
}
