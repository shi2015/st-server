package com.st.common.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * 读取项目相关配置
 */
@Data
@Component
@ConfigurationProperties(prefix = "st.token")
public class StTokenConfig
{
    private String header;
    private String secret;
    private long expireTime;
}
