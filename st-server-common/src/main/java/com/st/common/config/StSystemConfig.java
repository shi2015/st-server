package com.st.common.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * 读取项目相关配置
 */
@Data
@Component
@ConfigurationProperties(prefix = "st.system")
public class StSystemConfig
{
    /** 项目名称 */
    private String name;
    /** 版本 */
    private String version;
    /** 上传路径 */
    private  String profile;
    /** 文件最大限制 */
    private long maxSize;
    /** 头像最大限制 */
    private long avatarMaxSize;
    /** 获取地址开关 */
    private static boolean addressEnabled;

    public static boolean isAddressEnabled()
    {
        return addressEnabled;
    }
}
