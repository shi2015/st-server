package com.st.common.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * 读取项目相关配置
 */
@Data
@Component
@ConfigurationProperties(prefix = "st.contact")
public class StContactConfig
{
    private String name;
    private String url;
    private String email;
}
